DROP TABLE collections IF EXISTS;
DROP TABLE points IF EXISTS;

CREATE TABLE collections
(
    id   INTEGER IDENTITY PRIMARY KEY,
    name VARCHAR(50) UNIQUE NOT NULL
);
CREATE TABLE points
(
    id            INTEGER IDENTITY PRIMARY KEY,
    collection_id INTEGER NOT NULL,
    x             INTEGER NOT NULL,
    y             INTEGER NOT NULL
);

ALTER TABLE points
    ADD CONSTRAINT fk_points_collections FOREIGN KEY (collection_id) REFERENCES collections (id);
