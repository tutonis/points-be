package lt.tomas.points.service;

import lombok.AllArgsConstructor;
import lt.tomas.points.model.PointsCollection;
import lt.tomas.points.repository.PointsCollectionRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class PointsCollectionServiceImpl implements PointsCollectionsService {

    private final PointsCollectionRepository pointsCollectionRepository;

    @Override
    @Cacheable("collections")
    @Transactional(readOnly = true)
    public Optional<PointsCollection> findById(Long id) {
        return pointsCollectionRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PointsCollection> findAllPointsCollections() {
        return pointsCollectionRepository.findAll();
    }

    @Override
    @Transactional
    public PointsCollection savePointsCollection(PointsCollection collection) {
        return pointsCollectionRepository.save(collection);
    }

    @Override
    @Transactional
    public void deleteCollection(long id) {
        pointsCollectionRepository.deleteById(id);
    }
}
