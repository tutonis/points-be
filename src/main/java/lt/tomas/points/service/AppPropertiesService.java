package lt.tomas.points.service;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Data
@Validated
@Service
@ConfigurationProperties("app")
public class AppPropertiesService {

    @NotBlank
    private String ok;

}
