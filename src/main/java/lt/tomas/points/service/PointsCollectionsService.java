package lt.tomas.points.service;

import lt.tomas.points.model.PointsCollection;

import java.util.List;
import java.util.Optional;

public interface PointsCollectionsService {

    Optional<PointsCollection> findById(Long id);

    List<PointsCollection> findAllPointsCollections();

    PointsCollection savePointsCollection(PointsCollection collection);

    void deleteCollection(long id);

}
