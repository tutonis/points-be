package lt.tomas.points.utils;

import lt.tomas.points.model.Point;
import lt.tomas.points.model.PointsCollection;
import lt.tomas.points.web.api.model.PointsCollectionRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PointsCollectionUtils {

    private PointsCollectionUtils() {
    }

    public static PointsCollection requestToEntity(PointsCollectionRequest request) {
        PointsCollection collection = new PointsCollection();
        collection.setPoints(new ArrayList<>());

        return requestToEntity(collection, request);
    }

    public static PointsCollection requestToEntity(PointsCollection collection, PointsCollectionRequest request) {
        collection.setName(request.getName());

        List<Point> pointsNew = request.getPoints().stream()
                .map(PointsCollectionUtils::pointsToEntity)
                .collect(Collectors.toList());

        List<Point> points = collection.getPoints();
        points.removeIf(x -> true);
        points.addAll(pointsNew);

        return collection;
    }

    public static List<PointsCollectionRequest.PointRequest> getDuplicatePoints(List<PointsCollectionRequest.PointRequest> points) {
        return points.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() > 1L)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private static Point pointsToEntity(PointsCollectionRequest.PointRequest pr) {
        Point point = new Point();

        point.setX(pr.getX());
        point.setY(pr.getY());

        return point;
    }
}
