package lt.tomas.points.config;

import lombok.extern.slf4j.Slf4j;
import lt.tomas.points.service.AppPropertiesService;
import org.hsqldb.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

@Configuration
@Slf4j
@Profile("hsql")
public class HsqldbServerConfig {

    private static final String DEFAULT_DB_NAME = "testdb";

    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server loadHsqlServer(AppPropertiesService properties) {
        log.info("Starting hsqldb server");

        Server server = new Server();
        server.setLogWriter(this.slf4jPrintWriter());
        server.setNoSystemExit(true);
        server.setDatabasePath(0, "mem:" + DEFAULT_DB_NAME);
        server.setDatabaseName(0, DEFAULT_DB_NAME);

        return server;
    }

    private PrintWriter slf4jPrintWriter() {
        return new PrintWriter(new ByteArrayOutputStream()) {
            @Override
            public void println(final String x) {
                log.debug(x);
            }
        };
    }
}
