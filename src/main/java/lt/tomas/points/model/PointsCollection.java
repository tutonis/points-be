package lt.tomas.points.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "collections")
public class PointsCollection extends BaseEntity implements Serializable {

    public PointsCollection(Long id, @NotEmpty String name, List<Point> points) {
        super(id);
        this.name = name;
        this.points = points;
    }

    @Column(name = "name")
    @NotEmpty
    private String name;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "collection_id", nullable = false)
    private List<Point> points;

}
