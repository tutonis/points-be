package lt.tomas.points.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "points")
public class Point extends BaseEntity {

    public Point(Long id, @Min(-5000) @Max(5000) int x, @Min(-5000) @Max(5000) int y) {
        super(id);
        this.x = x;
        this.y = y;
    }

    @Min(-5000)
    @Max(5000)
    @Column(name = "x")
    private int x;

    @Min(-5000)
    @Max(5000)
    @Column(name = "Y")
    private int y;

}
