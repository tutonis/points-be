package lt.tomas.points.repository;

import lt.tomas.points.model.PointsCollection;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface PointsCollectionRepository extends Repository<PointsCollection, Long> {

    Optional<PointsCollection> findById(Long id);

    List<PointsCollection> findAll();

    PointsCollection save(PointsCollection collection);

    void deleteById(long id);

}
