package lt.tomas.points.web.api.model;

import lombok.Value;
import lt.tomas.points.model.Point;

@Value
public class PointResponse {

    private long id;
    private final int x;
    private final int y;

    public static PointResponse fromEntity(Point p) {
        return new PointResponse(p.getId(), p.getX(), p.getY());
    }
}
