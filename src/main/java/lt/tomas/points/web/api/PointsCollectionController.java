package lt.tomas.points.web.api;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lt.tomas.points.model.Point;
import lt.tomas.points.model.PointsCollection;
import lt.tomas.points.service.PointsCollectionsService;
import lt.tomas.points.utils.PointsCollectionUtils;
import lt.tomas.points.web.api.model.PointResponse;
import lt.tomas.points.web.api.model.PointsCollectionRenameRequest;
import lt.tomas.points.web.api.model.PointsCollectionRequest;
import lt.tomas.points.web.api.model.PointsCollectionSimpleResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/collections")
public class PointsCollectionController {

    private final PointsCollectionsService pointsCollectionsService;

    @GetMapping("/")
    public List<PointsCollectionSimpleResponse> findAllSimple() {
        log.info("Getting all collections");

        return pointsCollectionsService.findAllPointsCollections().stream()
                .map(PointsCollectionSimpleResponse::fromEntity)
                .collect(Collectors.toList());
    }

    @GetMapping("/{collectionId}/points")
    public List<PointResponse> findAllPoints(@PathVariable Long collectionId) {
        log.info("Getting all points of collection: {}", collectionId);

        List<Point> points = pointsCollectionsService.findById(collectionId)
                .map(PointsCollection::getPoints)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Collection not found. Id: " + collectionId));

        return points.stream()
                .map(PointResponse::fromEntity)
                .sorted(Comparator.comparingLong(PointResponse::getId).reversed())
                .collect(Collectors.toList());
    }

    /**
     * Just to test @ControllerAdvice as global alternative for ResponseStatusException
     */
    @GetMapping("/{id}")
    public PointsCollectionSimpleResponse findById(@PathVariable Long id) {
        log.info("Getting collection by id: {}", id);
        throw new EverythingIsWrongException("¯\\_(ツ)_/¯");
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public PointsCollection create(@Valid @RequestBody PointsCollectionRequest request) {
        log.info("Creating collection");

        List<PointsCollectionRequest.PointRequest> duplicates = PointsCollectionUtils.getDuplicatePoints(request.getPoints());
        if (!duplicates.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Found duplicate points" + duplicates);
        }

        PointsCollection collection = PointsCollectionUtils.requestToEntity(request);

        return pointsCollectionsService.savePointsCollection(collection);
    }

    @PostMapping("/{id}/rename")
    public void rename(@PathVariable Long id, @Valid @RequestBody PointsCollectionRenameRequest request) {
        log.info("Renaming collection: {}", id);

        PointsCollection saved = getById(id);
        saved.setName(request.getName());

        pointsCollectionsService.savePointsCollection(saved);
    }

    @PutMapping("/{id}")
    public PointsCollection update(@PathVariable Long id, @Valid @RequestBody PointsCollectionRequest request) {
        log.info("Updating collection: {}", id);

        PointsCollection saved = getById(id);

        PointsCollection collection = PointsCollectionUtils.requestToEntity(saved, request);

        return pointsCollectionsService.savePointsCollection(collection);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        log.info("Deleting collection: {}", id);

        pointsCollectionsService.deleteCollection(id);
    }

    private PointsCollection getById(Long id) {
        return pointsCollectionsService
                .findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Collection not found. Id: " + id));
    }
}
