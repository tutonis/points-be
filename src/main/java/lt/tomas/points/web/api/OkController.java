package lt.tomas.points.web.api;

import lombok.AllArgsConstructor;
import lt.tomas.points.service.AppPropertiesService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
public class OkController {

    private final AppPropertiesService appPropertiesService;

    @GetMapping("/ok")
    public String ok() {
        return appPropertiesService.getOk();
    }

}
