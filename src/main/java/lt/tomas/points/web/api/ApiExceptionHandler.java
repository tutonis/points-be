package lt.tomas.points.web.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Global exception handler, extends Spring base handler
 * Can map different exceptions and use quire resourceful Spring error message
 */
@Slf4j
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({EverythingIsWrongException.class})
    public void test(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_IMPLEMENTED.value());
    }
}
