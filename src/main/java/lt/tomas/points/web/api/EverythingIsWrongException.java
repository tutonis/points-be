package lt.tomas.points.web.api;

public class EverythingIsWrongException extends RuntimeException {

    public EverythingIsWrongException(String message) {
        super(message);
    }
}
