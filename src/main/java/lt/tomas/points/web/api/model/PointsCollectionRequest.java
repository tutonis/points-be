package lt.tomas.points.web.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PointsCollectionRequest {

    @NotBlank
    private String name;

    @Valid
    @NotNull
    private List<PointRequest> points;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class PointRequest {

        @Min(-5000)
        @Max(5000)
        @NotNull
        private Integer x;

        @Min(-5000)
        @Max(5000)
        @NotNull
        private Integer y;
    }
}
