package lt.tomas.points.web.api.model;

import lombok.Value;
import lt.tomas.points.model.PointsCollection;

@Value
public class PointsCollectionSimpleResponse {

    private final Long id;
    private final String name;

    public static PointsCollectionSimpleResponse fromEntity(PointsCollection collection) {
        return new PointsCollectionSimpleResponse(collection.getId(), collection.getName());
    }
}
