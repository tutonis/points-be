package lt.tomas.points.web.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lt.tomas.points.model.Point;
import lt.tomas.points.model.PointsCollection;
import lt.tomas.points.service.PointsCollectionsService;
import lt.tomas.points.utils.PointsCollectionUtils;
import lt.tomas.points.web.api.model.PointsCollectionRenameRequest;
import lt.tomas.points.web.api.model.PointsCollectionRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PointsCollectionController.class)
public class PointsCollectionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PointsCollectionsService pointsCollectionsService;

    private List<PointsCollection> collections;

    @Before
    public void init() {
        this.collections = setupCollection();
    }

    private List<PointsCollection> setupCollection() {
        List<Point> points = Arrays.asList(
                new Point(1L, 2, 8),
                new Point(2L, 1, 4),
                new Point(3L, 9, -15),
                new Point(4L, 55, -26)
        );

        return Arrays.asList(
                new PointsCollection(1L, "1 collection", points),
                new PointsCollection(2L, "blaaaaa", Collections.emptyList()),
                new PointsCollection(3L, "another1", Collections.emptyList())
        );
    }

    @Test
    public void shouldGet3Collections() throws Exception {
        given(pointsCollectionsService.findAllPointsCollections()).willReturn(collections);

        mvc.perform(get("/collections/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    public void findAllPoints() throws Exception {
        given(pointsCollectionsService.findById(1L)).willReturn(Optional.of(collections.get(0)));

        mvc.perform(get("/collections/1/points/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    public void findAllPointsWrongId() throws Exception {
        given(pointsCollectionsService.findById(999L)).willReturn(Optional.empty());

        mvc.perform(get("/collections/999/points/"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void createNoName() throws Exception {
        String name = "";

        PointsCollectionRequest request = new PointsCollectionRequest(name, Collections.emptyList());
        String json = new ObjectMapper().writeValueAsString(request);

        mvc.perform(post("/collections/")
                .content(json).characterEncoding(StandardCharsets.UTF_8.name())
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void createDuplicate() throws Exception {
        String name = "testCollection";

        List<PointsCollectionRequest.PointRequest> pointRequests = Arrays.asList(
                new PointsCollectionRequest.PointRequest(5, -10),
                new PointsCollectionRequest.PointRequest(5, -10)
        );

        PointsCollectionRequest request = new PointsCollectionRequest(name, pointRequests);
        String json = new ObjectMapper().writeValueAsString(request);

        mvc.perform(post("/collections/")
                .content(json).characterEncoding(StandardCharsets.UTF_8.name())
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void createNoPoints() throws Exception {
        String name = "testCollection";

        PointsCollectionRequest request = new PointsCollectionRequest(name, Collections.emptyList());
        String json = new ObjectMapper().writeValueAsString(request);

        mvc.perform(post("/collections/")
                .content(json).characterEncoding(StandardCharsets.UTF_8.name())
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void createOk() throws Exception {
        String name = "testCollection";

        List<PointsCollectionRequest.PointRequest> pointRequests = Arrays.asList(
                new PointsCollectionRequest.PointRequest(5, -10),
                new PointsCollectionRequest.PointRequest(6, -10)
        );

        PointsCollectionRequest request = new PointsCollectionRequest(name, pointRequests);
        String json = new ObjectMapper().writeValueAsString(request);

        PointsCollection collection = PointsCollectionUtils.requestToEntity(request);

        given(pointsCollectionsService.savePointsCollection(collection)).willReturn(collections.get(0));

        mvc.perform(post("/collections/")
                .content(json).characterEncoding(StandardCharsets.UTF_8.name())
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").hasJsonPath())
                .andExpect(jsonPath("$.name").hasJsonPath())
                .andExpect(jsonPath("$.points").hasJsonPath())
                .andExpect(jsonPath("$.points[0].id").hasJsonPath());
    }

    @Test
    public void renameNotFound() throws Exception {
        String name = "this is name";
        Long id = 999L;

        PointsCollectionRenameRequest request = new PointsCollectionRenameRequest(name);
        String json = new ObjectMapper().writeValueAsString(request);

        given(pointsCollectionsService.findById(id)).willReturn(Optional.empty());

        mvc.perform(put("/collections/" + id)
                .content(json).characterEncoding(StandardCharsets.UTF_8.name())
                .accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }
}
