# points-be

Spring boot web application, serving points data.

# db

For this demo project `hsqldb` was chosen as database engine. The data is being stored in memory,
also custom configuration was made to allow access from outside jvm process. This allows
external tools to connect to database which lifecycle is still managed by spring boot.

Connection config:
```
username: SA
password: 
url: jdbc:hsqldb:hsql://localhost/testdb
```

Database name was kept to default spring boot: `testdb`

# future development

0. cover with more tests
1. better errors handling - create own `ErrorResponse` model and leverage `ControllerAdvice` for
system wide exception handling
2. add response types, may be use `ModelMapper` as helper lib
2. gitlab ci/cd config - deploy to aws either `docker` image or simple jar to `Elastic Beanstalk`
3. better ops story - leverage spring actuator (or other tech) to monitor app
4. if needed/when growing extend using - `spring security`, `spring cloud config`, `spring session`,
`nginx/apache` as reverse proxy
6. `spring hateos`? - level 3 rest glory 

# launching app
You need to have `JAVA_HOME` set
```
* checkout repo
* ./mvnw clean install
* java -jar target/<app.jar>
```
